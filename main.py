# -*- coding: utf-8 -*-
import os

from flask import Flask, request, redirect, url_for
import csv

app = Flask(__name__)

UPLOAD_FOLDER = 'static/files'
app.config['UPLOAD_FOLDER'] =  UPLOAD_FOLDER

@app.route("/", methods=['POST'])
def uploadFiles():
      uploaded_file = request.files['file']
      if uploaded_file.filename != '':
           file_path = os.path.join(app.config['UPLOAD_FOLDER'], uploaded_file.filename)
           uploaded_file.save(file_path)
      return redirect(url_for('index'))

@app.route("/")
def traitement_csv():
    filename = request.args.get('filename')
    try:
        with open(filename, newline='') as csvfile:
            data = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for row in data:
                print(row)
    except:
        return request.close()
    return "tadam"

    def mean(list_value):
        mean = sum(list_value) / len(list_value)
        return mean

    def harmonic_mean(list_value):
        list_value.sort()
        if len(list_value) % 2 == 1:
            mediane = list_value[int(len(list_value) / 2)]
        else:
            mediane = (list_value[int(len(list_value) / 2) - 1] + list_value[int(len(list_value) / 2)]) / 2
        return mediane

if __name__ == "__main__":
    app.run(debug=True)