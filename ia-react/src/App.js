import { useState } from 'react';
import Table from './components/Table'
import { FileUploader } from "react-drag-drop-files";
import axios from "axios"

const fileTypes = ["CSV"];

function App() {

  const [file, setFile] = useState(null);
  const handleChange = (file) => {
    setFile(file);
  };

  async function handleClick() {
    if (file != null) {
      const formData = new FormData();
      formData.append("selectedFile", file);
      try {
        const response = await axios({
          method: "post",
          url: "/api/upload/file",
          data: formData,
          headers: { "Content-Type": "multipart/form-data" },
        });
      } catch (error) {
        console.log(error)
      }
    }
  }

  return (
    <>
      <div className="center">
        <h1>Decodoc</h1>
        <h3>Analyser des données n'a jamais été aussi simple !</h3>
        <FileUploader handleChange={handleChange} name="file" types={fileTypes} />
        <button onClick={handleClick} type="button" className="btn btn-primary">Envoyer</button>
        <Table />
      </div>
    </>
  );
}

export default App;
